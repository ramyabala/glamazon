import React, { Component } from 'react';
import {connect} from 'react-redux';
import logo from './glamazon.png';
import './App.scss';

import Selection from './components/Selection';
import OrderSummary from './components/OrderSummary';
import { updateItem } from './actions/itemActions';

class App extends Component {
  render() {
    return (
      <div class="HomePage">
        <div class="Header clearfix">
          <div class="Header__logo pull-left">
            <a href="/"><img src={logo} alt=""></img></a>
          </div>
          <div>
          <label for="shopping-cart"></label>
          <span class="cartQuantity">{this.props.itemsReducer.cartQuantity}
          </span>
          </div>
        </div>
        <div class="carousel">
          <div class="content-section">
            <div class="main_div">
              <h1 class="no-border">book beauty instantly,</h1>
              <h2 class="no-border">anytime, anywhere in australia</h2>
              <h3>Book a professional makeup artist, hair stylist, spray tanner, nail technician or massage therapist to you anywhere in Australia.</h3>
            </div></div>
        </div>
        <Selection updateSelectedItem={(itemName, qty)=>this.props.update(itemName,qty)}/>
        <OrderSummary items={this.props.itemsReducer.itemsSelected}/>
      </div>
    );
  }
}

const mapStateToProps = (state)=>{
  return{
    itemsReducer:state.itemsReducer,
    testReducer: state.testReducer
  };
};

const mapDispatchToProps = (dispatch)=>{
  return{
    update:(item, quantity)=>{
      dispatch(updateItem(item, quantity));
    }
  };
};

export default connect(mapStateToProps,mapDispatchToProps)(App);

//export default App;
