
import { createStore } from 'redux';
import{combineReducers} from 'redux';
import itemsReducer from './reducers/itemsReducer';
import testReducer from './reducers/testReducer';

const store = createStore(combineReducers({itemsReducer, testReducer}),{});

export default store;