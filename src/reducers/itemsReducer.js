const itemsReducer = (state = {
    itemsSelected: [],
    cartQuantity: 0
}, action) => {
    switch (action.type) {
        case "UPDATE":
            {
                var itemFound = state.itemsSelected.find(function (obj) {return obj.name === action.payload.name });
                
                if (itemFound !== undefined) {
                    console.log(state.itemsSelected.indexOf(itemFound));
                    state.itemsSelected.splice(state.itemsSelected.indexOf(itemFound), 1, { name: action.payload.name, quantity: action.payload.quantity });

                } else {
                    state.itemsSelected.push({ name: action.payload.name, quantity: action.payload.quantity });
                }
                
                var cartQty = 0;
                for (var x in state.itemsSelected) {
                    cartQty = cartQty + parseInt(state.itemsSelected[x].quantity);
                }
                state.cartQuantity = cartQty;
                break;
            }
        default:
            break;
    }
    return state;
};

export default itemsReducer;