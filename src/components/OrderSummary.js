import React, { Component } from 'react';
import '../App.scss';

class OrderSummary extends Component {
  render() {
    return (
      <div class="booking_section">
        <button class="selection_label">Order Summary</button>
        <div class="selection_panel">You are making a booking for</div>
          <ul>
          {this.props.items.map(item =>
            <li key={item}>
              {item.name} - {item.quantity}
            </li>
          )}
            </ul>
      </div>
    );
  }
}

export default OrderSummary;
