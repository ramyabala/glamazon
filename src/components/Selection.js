import React, { Component } from 'react';
import '../App.scss';
import './components.scss'

export class Selection extends Component {

  render() {
    return (
      <div class="booking_section">
        <button class="selection_label">Select a Service</button>
        <div class="selection_panel">
          <div>
            <input type="number" class="item_Quantity" min="0" max="100" 
            onChange={(e)=>this.props.updateSelectedItem("HairStyle-Color", e.target.value)}/>
            Hair Stylist(Color)
      </div>
          <div>
            <input type="number" class="item_Quantity" min="0" max="100" 
            onChange={(e)=>this.props.updateSelectedItem("Makeup", e.target.value)}/>
            Makeup
      </div>
          <div>
            <input type="number" class="item_Quantity" min="0" max="100" 
            onChange={(e)=>this.props.updateSelectedItem("SprayTan", e.target.value)}/>
            Spray Tan
      </div>
          <div>
            <input type="number" class="item_Quantity" min="0" max="100" 
            onChange={(e)=>this.props.updateSelectedItem("HairStyle-BlowDry", e.target.value)}/>
            Hair Stylist(Blow Dry)
      </div>
        </div>
      </div>
    );
  }
}

export default Selection;
